@extends('layouts/master')

@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-primary">
                <div class="card-header">
                <h3 class="card-title">Form Tambah Cast</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form action="/cast" method="POST">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama', '') }}" placeholder="Masukkan nama cast">
                        @error('nama')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="number" class="form-control" id="umur" name="umur" value="{{ old('umur', '') }}" placeholder="Masukkan umur cast">
                        @error('umur')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="bio">Bio</label>
                        <input type="text-area" class="form-control" id="bio" name="bio" value="{{ old('bio', '') }}" placeholder="Masukkan biodata cast">
                        @error('bio')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror                        
                        <!-- <input type="textarea" class="form-control" id="bio" name="bio" placeholder="Biodata cast"> -->
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">Tambah Cast</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection