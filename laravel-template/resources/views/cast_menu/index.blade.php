@extends('layouts/master')

@section('content')

<div class="card">
    <div class="card-header">
      <h3 class="card-title">Data Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @if (session('Success'))
          <div class="alert alert-success">
              {{ session('Success') }}
          </div>
      @endif
      @if (session('Delete'))
        <div class="alert alert-danger">
            {{ session('Delete') }}
        </div>
      @endif
      <a class="btn btn-primary mb-2" href="/cast/create"> Tambah Data Cast </a>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th style="width: 10px">No.</th>
          <th>Nama Cast</th>
          <th style="width: 20px">Umur Cast</th>
          <th>Biodata Cast</th>
          <th style="width: 190px">Aksi</th>
        </tr>
        </thead>
        <tbody>
            @forelse($cast_data as $key => $cast)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $cast->nama }} </td>
                    <td> {{ $cast->umur }} </td>
                    <td> {{ $cast->bio }} </td>
                    <td style="display: flex">  
                        <a href="/cast/{{$cast->id}}" class="btn btn-success btn-sm"> Lihat </a>
                        <a href="/cast/{{$cast->id}}/edit" class="btn btn-warning btn-sm"> Edit </a>
                        <form action="/cast/{{$cast->id}}" method="post">
                            @csrf
                            @method('DELETE')
                            <input type="submit" value="Hapus" onclick="return confirm('Yakin akan menghapus data?')" class="btn btn-danger btn-sm">
                        </form>
                    </td>
                </tr>
            @empty
            <tr>
                <td colspan="5" align="center"> Tidak ada data </td>
            </tr>
            @endforelse
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>

@endsection

@push('scripts-datatables')
  <!-- Data Tables Plugin -->
  <script src="{{asset('/template/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>    
@endpush