@extends('layouts/master')

@section('content')

<div class="card">
    <div class="card-header">
      <h3 class="card-title">Lihat Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th>Nama Cast</th>
          <th>Umur Cast</th>
          <th>Biodata Cast</th>
        </tr>
        </thead>
        <tbody>
            <tr>
                <td> {{ $cast_show->nama }} </td>
                <td> {{ $cast_show->umur }} </td>
                <td> {{ $cast_show->bio }} </td>
            </tr>
        </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>

@endsection

@push('scripts-datatables')
  <!-- Data Tables Plugin -->
  <script src="{{asset('/template/plugins/datatables/jquery.dataTables.min.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
  <script src="{{asset('/template/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
  <script>
    $(function () {
      $("#example1").DataTable();
    });
  </script>    
@endpush