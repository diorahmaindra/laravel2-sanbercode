<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class CastController extends Controller
{
    public function create() {
        return view('cast_menu/create');
    }

    public function store(Request $request) {
        $request->validate([
            'nama' => 'required|unique:cast',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast') -> insert([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"]
        ]);

        return redirect('/cast')->with('Success', 'Data berhasil ditambahkan!');
    }

    public function index() {
        $cast_data = DB::table('cast') -> get(); 
        return view('cast_menu/index', compact('cast_data'));
    }

    public function show($cast_id) {
        $cast_show = DB::table('cast')->where('id', $cast_id)->first();
        // dd($cast_show);
        return view('cast_menu/show', compact('cast_show'));
    }

    public function edit($cast_id) {
        $cast_edit = DB::table('cast')->where('id', $cast_id)->first();
        return view('cast_menu/edit', compact('cast_edit'));
    }

    public function update($cast_id, Request $request) {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('cast')
                    ->where('id', $cast_id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
        ]);

        return redirect('/cast')->with('Success', 'Data berhasil diupdate!');
    }

    public function destroy($cast_id) {
        $query = DB::table('cast')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('Delete', 'Data berhasil dihapus!');
    }
}
